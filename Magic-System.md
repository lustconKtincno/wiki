# Magic System

In Hereva (the world of Pepper&Carrot), the magic system is not a classic witchcraft's pentacle. It would be too banal, and Hereva hates banality.

Hereva's magic-system, or **"Chaos & Evolutions"** has evolved into six magic schools. They are represented on the Impossible Triangle diagram.

![Chaos&Evolutions](https://www.peppercarrot.com/0_sources/0ther/artworks/low-res/2015-06-08_magic-system-of-hereva_by-David-Revoy.jpg)
The "Chaos & Evolutions" impossible triangle ([full resolution here](https://www.peppercarrot.com/0_sources/0ther/artworks/hi-res/2015-06-08_magic-system-of-hereva_by-David-Revoy.jpg))

## Reading the impossible triangle :

The best way to read the Impossible Triangle is to view it on several axes: at the top is order, and the bottom is chaos. The left is spiritual and the right is material. With this we can read the positions of the different schools and better understand their affinities.

Starting from the bottom is **Chaosah**, harnessing the primal forces of chaos and physics. Moving to the left we see **Aquah**, the fusion of the untamed forces of chaos with the calm order of the spirit; much as water can turn from quiet and calm to rough and tumultuous. On the top left is **Ah**, representing the evolution into spiritual beings (beings requiring no physical form) and the order that comes from shirking off the material form. Moving right is **Hippiah**, the combination of harnessing the spiritual aspects of all living things and using them in order to enact the growth of living things. At the top-right is **Zombiah**, which views the spiritual as something to be harnessed to drive and animate the physical world. Moving down towards chaos is **Magmah**, which views the natural order as something to be changed and refined through the mixture of alchemy and chaos. 


## Chaosah :

Chaosah is at the base of the impossible triangle, and forms the basis of every other magic in Hereva. It's the house of **time, gravity, particle physics, and underground divine forces**. Some have postulated that Chaosah magic flows and controls all of the magic on Hereva in minute ways, but Chaosah practitioners tend to work in closed quarters and are loathe to share their magical secrets with outsiders. Chaosah magic, more than other magical systems, values practical magic over flashy displays (and instances where this value is not heeded have shown exceptional consequences). Chaosah magic can alter the shape and size of living beings, and can even open portals (referred to as "black holes") into worlds outside of Hereva. Chaosah does not operate as a traditional school. Students do not submit applications for enrollment; instead Chaosah prefers to use their own recruiting methods.

### Known members:  

-  Chicory (founder)
-  Thyme (actual master)
-  Cayenne (confirmed witch)
-  Cumin (confirmed witch)
-  Pepper (student witch)

### Chicory and the founding of Chaosah:

Chaosah was founded by a young witch named Chicory. She was a curious witch, and loved to tinker with magic. One night she was playing around and happened upon the "Chaosah Black Hole" spell (which is a signature spell for all Chaosah witches). She named her magic "Chaosah" because she felt she was channeling the chaos of Hereva in order to perform her magical incantations.

![ChicoryDiscoversBlackHole](https://www.peppercarrot.com/0_sources/0ther/wiki/low-res/2019-06-01_Chicory-Sketch_by-David-Revoy.jpg)
_Chicory discovers the Chaosah Black Hole (artist's rendition)_

Her discoveries caught the attention of other like-minded witches and she taught them what she had learned. Eventually more students arrived and Chicory found herself spending every waking moment teaching students. Her original plan was to have several of her brighter students teach the newer students, but the students wanted to hear the words of Chicory direct from the source. So she appointed her first apprentice. The apprentice is known as "the unnamed apprentice" in the Chaosah literature. This apprentice was eager to please Chicory. Chicory grew more and more frustrated and impatient the longer she was away from her research, and her apprentice tended to receive the results of her frustration and impatience.

One day Chicory needed something from the top shelf of one of the potion racks. "The unnamed apprentice" wanted to please an already frustrated Chicory, and decided to get her the potion as quickly as possible. Unfortunately the apprentice did not think there was enough time to get a ladder and instead decided to climb the wobbly potion shelves to get the potion. The shelves buckled under the weight, and the potions shattered and spilled all over "the unnamed apprentice". Several of the potions combined in strange and unpredictable ways, and "the unnamed apprentice" vanished. Even the records of the event still exhibit potion-induced time distortions and it is feared the true fate of "the unnamed apprentice" may never be known. This lead to one of the first rules of Chaosah: "A true witch of Chaosah doesn't climb the potion shelves".

After the potion incident Chicory selected someone to replace her (now absent) apprentice. Though she could no longer have a true apprentice (as the rituals surrounding apprenticeship are made for life, and the hope was the apprentice was still alive, somewhere) she selected an eager young witch named Thyme to take on the role. Thyme worked closely with Chicory to ensure that Chicory had more time to do her research, but despite Thyme's best efforts to wall off her mentor there were still folks who demanded they receive instruction from Chicory herself. The demands of running the school and "being the leader of Chaosah" took their toll on Chicory. Legend has it that Chicory decided she had completed her teaching of the Witches of Chaosah and decided to seclude herself in a pocket dimension where she could continue her research undisturbed. Whether she will ever return is the stuff of speculation and legend. Before the demise of Chaosah there were a small band of witches who would hold a vigil waiting for the eventual return of Chicory. Those who were closest to Chicory (including Thyme) figured it was best to let her do her research in peace, lest she return in a more powerful and agitated state.

Thyme took on the duties of running the school but some students felt she lacked the authenticity of Chicory. Gradually those students drifted away to other schools. Thyme resented their departure, and that resentment fueled her late nights of research into Chaosah Magic. Some say this period of research was greater than anything Chicory had published in the formative years of Chaosah. There are more manuscripts with Thyme's handwriting on them than any other Chaosah witch to date. Thyme even perfected the micro dimension through trial and error. Only foolish witches make the connection to Thyme's experiments in the micro dimension and Thyme's current stature.

Gradually the school of Chaosah became more secretive and secluded as the students departed. The school did minimal recruiting effort to keep the school going. There were many periods where Chaosah would be very selective, and periods where Chaosah would take anyone that wanted to come. One thing that all of the students had in common was the ability to withstand Thyme's fits of rage. Only Cumin has managed to withstand enough of Thyme's rants and threats long enough to become Thymes apprentice. Whether Thyme made Cumin an apprentice as a favor to someone, or just to get Cumin to stop nagging her is anyone's guess. Some Chaosah witches wondered if Cayenne would have been Thyme's apprentice had Cumin not been accepted before Cayenne arrived. (They also wondered if Thyme kept sending Cumin on dangerous missions in an effort to get Cayenne the apprenticeship promotion.)

### Burying failures:

A curious habit of Chaosah is the habit of burying items (ep. 12, 19). Chaosah has a long-standing tradition of taking their physical mistakes and literally burying them in the ground. This practice traces its origins back to Chicory, the founder of Chaosah magic. An unnamed student approached her with a potion or a spell that did not work (the tradition speaks of both spells and potions, depending on who tells the story). Chicory looked at the potion / spell and declared "This wasn't worth the Rea it took to make it. Perhaps you can reclaim some of the Rea you wasted in making it in the effort you'll use to bury it". As Chicory was a wise and feared teacher of Chaosah these words became the wisdom that any project that had no hope of recouping the cost involved in making it would be buried, both to reclaim the Rea used in creating it and as a deterrent to creating such foolishness in the future. Usually the creator is tasked with burying their failed projects and experiments, although there was one class that so displeased Chicory that she made the whole class dispose of each other's failures as punishment.

Even a failed Chaosah experiment can be quite powerful, and burying them can lead to unintended consequences. Potions can leak, spells can be read by unskilled practitioners, and defective talismans can wreak all sorts of havoc. Worse, these are the ones that Chaosah witches deem are not good enough to keep around. A failed potion labeled "growth potion" could shrink you, make every other part of you grow, or cause your hair to fall out (and those are the best case scenarios). This is of no concern to Chaosah witches who deem such interactions with their failures as happy little accidents.

### Chaosah and The Great War:

During the great war all of the members of Chaosah were wiped out by the forces of Ah. This upset the balance of Hereva and caused The Great Tree of Komona to perish, leaking Rea throughout the land. Realizing Chaosah was an integral part of the magic of Hereva the adepts of Ah recruited Zombiah to resurrect enough of Chaosah to train an heir. Only three of the witches of Chaosah were able to be adequately resurrected: Cayenne, Thyme and Cumin. Once the witches of Chaosah returned the balance of Hereva was restored. As part of their return they are charged with producing an heir for the school of Chaosah as the terms of their resurrection prevents them from continuing it themselves.

### A True Witch of Chaosah:

* "[is a] mean witch of Chaosah." (ep. 11)
* "[commands] fear, obedience and respect." (ep. 11)
* "...has influence over the powerful!" (ep. 11)
* "...does not use magic for daily chores." (ep. 12)
* (regarding frivolous potions) "...wouldn't create these kinds of potions." (ep. 12)
* "... needs neither compass nor map. A starry sky will suffice." (ep 17)

### Other wisdom of Chaosah:

* "A good dark stare is worth a dozen curses! [...] "Stare them down. Dominate them!" -- Cayenne (relayed by Pepper in ep. 17)
* "To know the edible plants is to know where to find ready-made potions against hunger!" -- Cumin (relayed by Pepper in ep. 17)


## Magmah :

Magmah is at the middle-right of the triangle, and like Hippiah is considered a major school of magic. It's the house of **cooking, baking, grilling, boiling, frying, steaming, toasting** but also is the house of **alchemy and rare metals**. Like Hippiah it is a well documented magic school, though not as open as Hippiah. Adepts of Magmah tend to use a blend of Hippiah and Magmah magic to create amazing dishes, as well as strong materials for other magical schools (notably Zombiah, which uses those materials for their animating purposes.) Magmah is a part of daily-life, and has found uses in both the home as well as for wars and industry. During the great crisis the school of Magmah realized (along with the school of Hippiah) that they could do more good in teaching a diluted form of their magic to the peoples of Hereva. Because of this most folks on Hereva know a small portion of Magmah, though the difference between a dabbler and an adept is huge. Magmah hosts an occasional contest for determining who has best mastered the techniques of Magmah. Competition is fierce and those who make it through the rigorous tests and trials are considered for deeper studies.

### Known members:  

-  Savory, Mint, and Chive (catering founders of Magmah)
-  Heliantha (magical school founder)
-  Vanilla (actual master)
-  Paprika (confirmed witch)
-  Saffron (student witch)
-  Tabasco (student witch)
-  Espelette (student witch)
-  Cubanelle (student witch)
-  Aji (student witch)
-  Pasilla (student witch)

### Magmah and The Great War

The Great War started as a result of an argument between Magmah and Aquah. As part of the treaty (and as punishment for starting the war) the school was renamed "Kielbasah" by Aquah for a period of 10 years after the war. "Kielbasah", Aquah explained, "is the smell we closely associate with all of Magmah". All records at the time were magically altered to reflect the change. There are still folks who have trouble remembering what to call Magmah today


## Aquah :

Aquah is at the middle-left of the triangle, and it's also a major school. It's the house of **water, wind, clouds and the abyss**. Little is known of this school as the chief requirement is to learn how to breathe underwater to even attempt to attend courses. This is acceptable to Aquah, as they are content to commune amongst themselves and the denizens of the sea. Rarely do they interact with those on land, and those who fail to meet the standards for the Aquah  school find themselves washed ashore (or rather someone else finds them). As they tend to be out-of-sight, out-of-mind for most land folk it is easy to forget this school, but the wise know it is foolhardy to discount them.

### Known members:  

-  Unknown mermaid (founder, actual master)
-  Spirulina (student witch)

### Aquah and The Great War

Aquah was responsible for escalating the hostilities in the great war. As part of the treaty (and as punishment for retaliation) the school was renamed "Wharrgarblah" by Magmah (as in the sound one makes when one is drowning). This too lasted ten years, according to the treaty (though some in Magmah feel the punishment was too light as few have ever said "Wharrgarblah" to an Aquah practitioner's face.)


## Zombiah :

Zombiah is at the top-right of the Impossible Triangle, and it's one of the three sub-schools of "Evolution". It's the most curious of the houses, finding itself at the intersection of the macabre (**death, zombies, and darkness**)  and transformation (**recycling, re-use, and animation/reanimation**). Zombiah is not specifically about raising the dead, but rather concerns itself with bringing out the innate movement in all things. It wasn't until centuries of economic strife that Zombiah was applied to formerly living creatures.

While Zombiah can be used for reanimating formerly living creatures it's main use is for the machinery of Qualicity.  Zombiah requires that resurrection only happen if the spirit is willing. (It's unclear to outsiders how this negotiation between body and spirit occurs, but Zombiah insists it does happen.) There is a quasi-acceptance between adherents of the school of Ah and Zombiah. Adherents of Zombiah view their magic as just another tool to help aid in making lives easier through automation, and prolonging the usefulness of the body by bringing the spirit back if it is willing. Ah followers contend the body must rest once the spirit takes leave, lest the spirit forever wish to remain within it's former home. Numerous times have the philosophers of Hereva tried to address this question of the connection of body and spirit. Some believe that only followers of Ah know the answer but the school of Ah refuse to participate in the discussion. The practice of Zombiah continues, though primarily on mechanical constructs and the dwindling few who will themselves into resurrection. There are signs that part of the treaty from The Great War resulted in the curtailing of the use of Zombiah on living beings but those details are kept hidden in a magical lock box that few can access. 

Adepts of Zombiah tend to mingle their magic with Magmah in order to craft better materials to animate. Those that have mastered both can create amazing clockwork machinery that mimics life in minute detail. Zombiah magic has been used to create machines such as robot butlers, electrical generators, factory assembly lines, and the like. Some folks in Hereva are still uneasy around moving, non-living constructs. Some have described Zombiah's constructs as uncanny and eerie, though some find them charming and endearing. This has drawn mixed responses from the other magic schools who wonder if such constructs are capable of independent thought. Some folks speculate that the reason Ah continues to monitor Zombiah is to see if these constructs might have a spiritual existence. 



### Known members:  

-  Apiaceae (founder)
-  Anemone (Apiaceae's sister and co-founder)
-  Prince Regent Monarda (first ruler to welcome Zombiah magic into his court)
-  Soumbala (actual master)
-  Coriander (student witch)

### Zombiah and The Great War

At the end of the war when Ah destroyed Chaosah it was Zombiah who brought back the three members of Chaosah. This has lead some to question whether Ah changed the terms of the treaty after Zombiah helped Ah restore balance. 


## Hippiah :

Hippiah is at the top-middle of the impossible triangle, and it's the central sub-school of 'Evolutions' magic. It's the school of **plants, creatures, insects** (yes, even pests like mosquitoes) and **all living things**. Hippiah is one of the most highly regarded schools of Evolution magic, as it is the school most willing to share its knowledge. Hippiah documents farming, gardening and fertility in great depth, serving as a guide for most of the farmers of Hereva. Although Hippiah was always an open school it realized during the great crisis that it could not conscientiously keep its secrets and began spreading its knowledge throughout Hereva. While farmers know a bit of Hippiah magic the difference between a Hippiah adept and a mere practitioner is staggering. Few outside the Hippiah schools have seen the full extent of Hippiah magic, though there are stories of vegetables and grain that are so tasty as to make even the schools of Magmah weep with joy. Hippiah magic techniques are so popular they tend to seep into other school's curriculum, though those schools tend to ignore the source and claim it as their own.

### Known members:  

-  Dandelion (founder)
-  Botanic (former master)
-  Basilic (actual master)
-  Quassia (teacher)
-  Millet (former teacher)
-  Camomile (student witch)
-  Cardamom (student witch)
-  Cinnamon (student witch)

### Hippiah and The Great War

Hippiah did not play an active role in The Great War. Their role was more in trying to heal up the land, and provide food to those areas blighted by the destruction. Their work continues today.

### Hippiah is:

* "... not a magic for making weedkillers" (ep18)


## Ah :

Ah is at the top-left of the triangle, and it's the last sub-school of 'Evolutions'. It's the house of the **after-life, ghosts, souls, and is the open door to parallel universes**. Because Ah deals with spirits and other planes of existence its secrets are carefully guarded. Many myths and legends about Ah have arisen because of such secrecy, and the silence does little to disprove such wild conjecture. Some say that the spirit world might someday unravel the fabric of Hereva itself, or that they guard the secrets to eternal life, while others say the reason Ah is practiced in secret is because the spirits are selective with whom they communicate and are easily angered with banal questions about future events. Whatever the truth, it is easier to find rumors of Ah than actual written documentation as Ah strictly prohibits documenting their practices, preferring instead to pass them on through oral traditions to trusted adepts. Adherents of Ah choose their members carefully (some say the spirits themselves select who shall learn the ways of Ah), and there are no accounts of outsiders learning Ah. Practitioners of Ah travel in small nomadic bands, preferring solitude over any company. Perhaps the spirits are always guiding, talking, and listening so the followers of Ah are never truly alone. There are many adepts of Ah, each wandering the lands of Hereva, and it is considered good fortune to see an adept of Ah. Rarely do they stay for long, guided by invisible winds in unknowable directions.

Occasionally adepts of Ah leave markings on their journey for the others to find. What these markings say is still a mystery but those who attempt to deface the markings have regretted their careless decision.

Ah is primarily located in the land of the setting moons, which not only refers to the location where the moons of Hereva set, but also the dragons who find comfort in their last days. Ah and the dragons live peacefully together; the dragons finding comfort in a spiritual place, and Ah in guiding the spirits of the dragons to the heavens.

Ah uses a complex system of robe styles and decorative bone hairpieces in order to convey status. Most of Ah's many adepts wear red with decorative white bones. Only the knights of Ah wear bones that have been tinted black through a magical ceremony. Wasabi, the leader of Ah, wears a distinctive white robe with a complex black bone hairpiece adorning the back of her hair. Wasabi's white robes signify her level of connection with the spirit world. Only Wasabi is permitted to wear the white robes.

There are several known levels of adepts for Ah. The first are the initiates. These are adepts that are taught in the school through communal learning. They do not leave the school during this period. The initiates are tasked with many filing and processing duties while in the school, though they are not permitted in the more classified areas of the school. Initiates who prove themselves worthy graduate and become wanderers. These are the most commonly found members of Ah in Hereva. They travel throughout Ah with minimal provisions on various journeys lead by either the spirits or Wasabi (or both). Some adepts travel over Ah on young dragons. These riders are selected by the dragons and form a special bond between dragon and rider. These riders are given special privileges in the school of Ah.

The most secretive and exclusive level of Ah are the Knights of Ah. These are a select few adepts of Ah that show exceptional loyalty to Wasabi and demonstrate a high level of aptitude within the school. It is the highest honor of any member of Ah to be chosen to be a Knight of Ah. Most Knights require decades of service to Ah to prove themselves worthy of the honor. Shichimi is the only Knight of Ah to receive this honor at such a young age.


### Known members:  

-  Sansevieria (founder)
-  Wasabi (actual master)
-  Shichimi (student witch and knight of Ah)
-  Torreya (dragon rider)

### Ah and The Great War

Ah was responsible for the destruction of Chaosah during the great war. What ended the war almost ended Hereva itself, and Ah realized without Chaosah the world of Hereva would suffer a magical imbalance that would drain Hereva of magic itself. They enlisted a chastened Zombiah to resurrect the viable members of Chaosah, and keep a close eye on Chaosah and their attempts at securing an heir. Using Zombiah again to resurrect Chaosah will never happen again if Ah has any say in it.

### A True Witch of Ah: 

* (relating to comfort) "...must not live in that fashion" (ep. 13)


## Magical Unit (Rea)

Pepper and Carrot inhabit the magical land of Hereva. Hereva was born from the magical forces known as "Chaos & Evolutions". "Chaos & Evolutions" (as they are known) exist within the cosmos and Hereva formed when the magic elements of the cosmos evolved from the chaos. Herava's magical nature permeates it so thoroughly that any practitioner of magic will have little difficulty in finding the resources to manipulate some form of magic. Magic is not without discipline though, and practitioners of magic must undergo specialized training in order to properly channel the magical energies within the world. 

Magic in Hereva is unlike our concepts of Qi and Mana. Because magic formed the universe it is called "Rea", which is an abbreviation of "Reality". Rea can be loosely thought of as collecting the by-products of a task. By concentrating on making a potion for a loved one the practitioner can harness some of the Rea used in creating the potion. Conversely if there is no attachment, or if the practitioner is careless then the Rea is lost and the Rea must be obtained via other means. 

Rea can be harnessed by the following methods:

- The process of summoning
- The time or duration while making something
- Attention, Dedication, or Care
- Emotional engagement

Rea is used for all aspects of life on Hereva. Farmers in the village of Squirrel's End use Rea to grow healthier crops (and tastier crops, as the locals will attest). They also use Rea to ward off the denizens of the forest (which also attest to the tastiness of the crops in Squirrel's End). Most Witches of Hereva don't use their Rea for mere "chores", preferring instead to save their Rea use for more important matters. This is why you'll find witches doing things like sewing on a button or purchasing starfruit from market rather than conjuring up a new outfit or summoning starfruit: the cost in replenishing the Rea outweighs the benefit.

Most practitioners don't actively monitor their Rea usage. They can sense when they are nearing depletion and can seek out ways to regenerate or obtain Rea as needed. How much Rea a person can maintain / channel varies greatly. Some folks are able to charge up luminous levels of Rea. A few witches are skilled in channeling and manipulating large amounts Rea without the need to build up reserves. Such channeling can be dangerous, though; while it is difficult to get too much Rea it is possible certain spells can consume all of the Rea from the caster and the surrounding area. Such spells can lead to disastrous results.

Training in one of the schools of magic allows for more controlled and focused use of Rea. Without this training Rea becomes more of a "guided intention" rather than a direct command of Rea. By careful study, focus, and desire one can learn to harness Rea to conform to their will. As the Herevans learned how to control Rea they quickly realized this power could cause havoc if not properly taught, and formed the schools of magic to properly teach the various ways of harnessing Rea. They perfected their craft via trial and error, and compiled that information into spellbooks and potion recipes. Some schools became more secretive about their methods for controlling Rea, while others remained open to all students (mostly from necessity, but some because of the charters of their founding members). Most of the people of Hereva know a little bit of the schools of magic they were exposed to (for instance the farmers of Hereva know a diluted form of Hippiah magic passed on from one generation to the next) but there is a huge difference between mimicking the magic of a school and knowing all of their secret knowledge.

Magmah, Aquah, and Hippiah are especially tuned to channel Rea into the physical world. Ah and Zombiah focus their Rea outside of the physical world. Chaosah is attuned to both the physical and non-material world.

Rea must be replenished from time to time. While Rea can be replenished in various ways (meditation, careful focus on a project, dedication to a project, etc.) these take time and effort to achieve. Certain potions and rare ingredients can be purchased to replenish Rea, but these can be quite costly and are used by a limited number of practitioners. Beware anyone who claims to have invented a way to regenerate Rea without effort; such "perpetual Rea machines" are usually little more than "get-Ko-quick" schemes.

Rea can be transferred, for example in episode 36 a dragon transfers Rea to Pepper. There are essential differences between normal Rea and dragon Rea though, which causes Pepper to have to seek the healing powers of the tears of the Phoenix in episode 37.
